{ pkgs ? import <nixpkgs> {}
, ghc ? "ghc864"
}:

pkgs.stdenv.mkDerivation rec {
  name = "compass-server-ldap";

  buildInputs = [
    pkgs.zlib
    pkgs.haskell.compiler.${ghc}
    pkgs.which
    pkgs.cabal-install
  ];

  shellHook = ''
    export LD_LIBRARY_PATH=${pkgs.lib.makeLibraryPath buildInputs}:$LD_LIBRARY_PATH
    export LANG=en_US.UTF-8
  '';
}
