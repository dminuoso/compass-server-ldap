{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}

module Compass.Ldap.Pool
  ( Pool
  , withLdap
  , Ldap
  , LdapH
  , createPool
  )
where

import Data.Text (pack)
import Control.Monad
import Control.Monad.IO.Class (liftIO)
import Data.Pool
import Ldap.Client
import Control.Monad.Logger
import UnliftIO (MonadUnliftIO, withRunInIO)
import UnliftIO.Exception

-- | Runs an action needing an Ldap handle using a connection pool.
withLdap :: MonadUnliftIO m
         => MonadLogger m
         => Pool LdapH
         -> (Ldap -> m a)
         -> m a
withLdap pool act = withRunInIO $ \io -> do
    io $ $logDebug "Taking LDAP handle out of the pool"
    (ldapH, local) <- liftIO (takeResource pool)

    let
      remove = io $ do { $logDebug "Remove LDAP handle from the pool"
                       ; liftIO (destroyResource pool local ldapH) }

      add    = io $ do { $logDebug "Add LDAP handle back the pool"
                       ; liftIO (putResource local ldapH) }

    res <- liftIO (runsInEither (io . act) ldapH) `onException` add
    case res of
      Right a -> add *> pure a
      Left err -> do { io $ $logError "Error caught from inside LDAP thread"
                     ; io $ $logError (pack (displayException err))
                     ; remove
                     ; throwIO err }
