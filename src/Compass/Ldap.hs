{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

module Compass.Ldap
  ( LdapStrat(..)
  , ldapAuthenticate'
  , ldapAuthenticate
  , NotFound(..)
  )
where

import           Data.Monoid

import           Compass.Authentication
import           Compass.Env
import           Compass.Handler
import           Control.Monad.Except
import           Control.Monad.Logger

import           Data.Text
import           Data.Text.Encoding
import           Data.Text.Encoding.Error
import           Ldap.Client
import qualified Ldap.Client as L (Password(..))
import           Ldap.Client.Bind (bindEither)
import           Ldap.Client.Search (search)
import           Optics
import           UnliftIO.Exception

import           Network.RADIUS.Lens
import           Network.RADIUS.Types

data LdapStrat = LdapStrat
  -- ^ The base DN the users are at
  { baseDn :: Text

  -- ^ What attribute is used in LDAP to identify a user
  , idAttr :: Text

  -- ^ A Getter to determine the username we should look up
  , userLens :: AffineFold Request Text
  }

-- | Authenticate a user against LDAP and return the users LDAP entry.
-- Rejects on failure. For a non-rejecting variant see 'ldapAuthenticate'.
ldapAuthenticate' :: LdapStrat -> Ldap -> Rad 'Access SearchEntry
ldapAuthenticate' s l = rejectOnFailure (ldapAuthenticate s l)


-- | Authenticate a user against LDAP and return the users LDAP entry.
ldapAuthenticate :: LdapStrat -> Ldap -> Rad 'Access (Either Text SearchEntry)
ldapAuthenticate strat ldap = runExceptT $ do
  user <- gview (userLens strat) `note` "No username specified"
  pass <- utf8 <$> lift papPassword `note` "No password specified"
  authenticate (dn user) pass ldap
  lift (getEntry strat user ldap)

  where
    utf8 = decodeUtf8With lenientDecode

    dn :: Text -> Dn
    dn user = Dn ((idAttr strat) <> "=" <> user <> "," <> (baseDn strat))

    note :: MonadError e m => m (Maybe a) -> e -> m a
    note act e = maybe (throwError e) pure =<< act

rejectOnFailure :: Rad 'Access (Either Text a) -> Rad 'Access a
rejectOnFailure act = either immediateReject pure =<< act

authenticate :: Dn -> Text -> Ldap -> ExceptT Text (Rad 'Access) ()
authenticate dn pass ldap = do
        logDebugN ("Attempting to bind with " <> pack (show dn))
        r <- liftIO (bindEither ldap dn (L.Password pass'))
        case r of
            -- Authentication failure
            Left (ResponseErrorCode _req InvalidCredentials _dn _text)
                    -> do logDebugN "Bind failed, wrong password."
                          throwError "Bad credentials"

            -- If any other LdapError is thrown, lift it into an IO exception.
            Left e   -> throwIO e

            -- Authentication succeeded
            Right () -> do logDebugN "Bind succeeded"
                           pure ()
    where
      pass' = encodeUtf8 pass

getEntry :: (MonadLogger m, MonadIO m)
         => LdapStrat -> Text -> Ldap -> m SearchEntry
getEntry strat user ldap = do
    m <- liftIO (search ldap base smod sfilter attrs)
    case m of
      [] -> throwIO (NotFound user)
      [x] -> pure x
      _xs -> do $logError "Multiple results despite limit of 1"
                throwIO (InvariantViolation ("Multiple users found for" <> user))
  where
    -- | Only list one entry
    smod    = size 1

    -- | The LDAP filter expression
    sfilter = Attr (idAttr strat) := (encodeUtf8 user)

    -- | The base DN we search in. We look in ou=active instead of ou=inactive
    -- because inactive accounts do not exist for the purpose of
    -- authentication.
    base   = Dn (baseDn strat)

    -- | No extra search attributes need to be passed to LDAP
    attrs  = []

data InvariantViolation = InvariantViolation Text deriving (Eq, Show)
instance Exception InvariantViolation

data NotFound = NotFound Text deriving (Eq, Show)
instance Exception NotFound
